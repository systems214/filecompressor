#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<fcntl.h>
#include"utils.h"
#include"compress.h"


int compress(int fd, char* filename, int codebook) {

	//Checks if the file descriptor exists/works, and also just skips the first character, cuz we dont care about escape char, and the new line after it
	char* str = (char *) malloc(sizeof(char)*2);
	char* delim = (char *) malloc(sizeof(char)*2);
	char* huffDelim = NULL;
	char* huffToken = NULL;
	char* temp = NULL;
	int fileNameSize = strlen(filename) + 5;
	char* compressedName = (char *) malloc(sizeof(char)*fileNameSize);
	memset(str,'\0',2);
	int index = 0;
	int max = 2;
	char buff = 0;
	int fdCompressed = 0;
	int i = 0;

	//If it does not exists, or error, returns error
	if(fd <= 0 || codebook <= 0) {
		free(compressedName);
		free(str);
		free(delim);
		return 0;
	}
	if(filename[strlen(filename)-4] == '.' && filename[strlen(filename)-3] == 'h' && filename[strlen(filename)-2] == 'c' && filename[strlen(filename)-1] == 'z') {
		free(str);
		free(delim);
		free(compressedName);
		return 0;
	}
	//Zero out compressedName
	memset(compressedName,'\0',fileNameSize);
	//Create the name of the compressed file we are making
	strcat(compressedName,filename);
	strcat(compressedName,".hcz");

	//Create the file that we will write the compress data to
	fdCompressed = creat(compressedName,0640);
	if(fdCompressed < 0) {
		return 0;
	}
	//We have no need for the name of the file anymore, so free it (it is still opened though)
	free(compressedName);

	loadC(codebook);
	//While there are still characters in the file left
	while(read(fd,&buff,1) > 0) {
		//Reading the original Document
		//If there is no delim, then continue adding onto str (the token/word we are compressing)
		if(iscntrl(buff) == 0 && buff != ' ') {
			str[index] = buff;
			index++;
			//Check if str has reached it maximum allocated bytes
			if( (index+1) >= max) {
				max = max * 2;
				//Which will then proceed to double the size, and realloc it
				temp = realloc(str,max);
				while(temp == NULL) {
					printf("Waiting for realloc to return\n");
				}
				str = temp;
				temp = NULL;
				//And it will set the remaining bytes that were realloced to null terminators
				memset(str+(index+1), '\0', max-(index+1));
			}
		}
		//A delim has been found, so the token/word is complete
		else {
			//We set up our "delim" token, because the delim will also need to be compressed
			delim[0] = buff;
			delim[1] = '\0';

			//Check to see if said token actually was created because it is possible that it is just repeated delimiters
			if(index != 0) {
				grabHuffC(str,fdCompressed);
			}
			//Search for the huffman code equivalent of the delim
			grabHuffC(delim,fdCompressed);								
			//Reset our token and delim
			memset(str,'\0',max);
			memset(delim,'\0',2);
			index = 0;
			//Continue to find the next word
		}
	}

	i = 0;
	for(i = 0; i < cSize; i++) {
		if(cbookToken[i] != NULL) {
			free(cbookToken[i]);
			cbookToken[i] = NULL;
		}
		if(cbookCode[i] != NULL) {
			free(cbookCode[i]);
			cbookCode[i] = NULL;
		}
	}
	if(cbookToken != NULL) {
		free(cbookToken);
		cbookToken = NULL;
	}
	if(cbookCode != NULL) {
		free(cbookCode);
		cbookCode = NULL;
	}
	
	//Close all our file descriptors and free all the malloc strings we made
	close(fd);
	close(fdCompressed);
	if(delim != NULL) free(delim);
	if(str != NULL) free(str);
	return 1;
}

#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<fcntl.h>
#include<sys/stat.h>
#include"build.h"
#include"utils.h"
#include<ctype.h>

void rehash();
int insert(char* token);
void insert_heap(node n);
int search_heap(node);
void sift_up(node);

void fix_heap(int);
node pop();

void print_heap(){
	int j=0;
	while(j<null_spot){

		printf("%s %d\n", min_heap[j]->string, min_heap[j]->freq);
		j++;
	}
	printf("Heap has %d tokens: \n", j);
}

int build_table(int fd) {

//	printf("A test string to see if build ran\n");
	char* str = (char *) malloc(sizeof(char)*2);
	char* delim = NULL;
	char* temp = NULL;
	memset(str,'\0',2);
	int index = 0;
	int max = 2;
	char buff;
	int i;
	
	while( read(fd, &buff, 1) > 0) {
		if(iscntrl(buff) == 0 && buff != ' ') {
			str[index] = buff;
			index++;
			if( (index+1) >= max) {
				max = max *2;
				temp = realloc(str,max);
				while(temp == NULL) {
					printf("Waiting for realloc to return\n");
				}
				str = temp;
				memset(str+(index+1),'\0',max-(index+1));
			}
		}
		else {

			delim = (char*) malloc(sizeof(char)*2);
			memset(delim,'\0',2);
			delim[0] = buff;
			delim[1] = '\0';
			if(insert(delim) == 2) {
				free(delim);
				delim = NULL;
			}

			if(index != 0) {
				temp = realloc(str,index+1);
				while(temp == NULL) {
					printf("Waiting for realloc to return\n");
				}
				temp[index] = '\0';
				str = temp;
				if(insert(str) == 2) {
					free(str);
					str = NULL;
				}
				max = 2;
				index = 0;
				str = (char *) malloc(sizeof(char)*2);
				temp = NULL;
			}
		}
	}
	//printf("%d\n", index);
	if(str != NULL && index == 0){
		free(str);
	}else{
		insert(str);
	}
	if(read(fd, &buff, 1) < 0) {
		close(fd);
		return 0;
	}
	
	close(fd);
	//*/
	
	return keys;
}

int build_heap(){
	//fill heap
	node ptr = head;
	while(ptr != NULL){	
		insert_heap(ptr);
		ptr = ptr->left;
	}
	
	int i;
	//reset all branch pointers for building tree
	//and relink to "next" for priority queue
	for(i=0; i<null_spot; i++){
		if(min_heap[i]->left != NULL){
			min_heap[i]->left = NULL;
		}
		if(min_heap[i]->right != NULL){
			min_heap[i]->right = NULL;
		}
	}
	//zero out array spots after last heap element
	for(i=null_spot; i<heap_size; i++){
		min_heap[i] = NULL;
	}

}

int write_codebook(int length){
	int fd = open("HuffmanCodebook", O_RDWR | O_CREAT | O_TRUNC, 0640);
	
	int count = 0;
	//control char delimiter
	write(fd, "~~~\n", 4);
	
	int i;
	for(i=0; i<length; i++){
		
		//control char boolean
		int ctrl = iscntrl(code_arr[i]->string[0]);
		
		//buffer length. 3 for \n, \t, and \0 plus code length
		int buff_len = 3 + strlen(code_arr[i]->code);
		if(ctrl > 0){
			//+4 for length control char delimiter and char
			buff_len = buff_len + 4;
		}else{
			//regular string
			buff_len = buff_len + strlen(code_arr[i]->string);
		}
		
		//buffer to print
		char* buff = (char*) malloc(sizeof(char)*buff_len);

		buff = strcpy(buff, code_arr[i]->code);
		buff = strcat(buff, "\t");
		
		//cat all the things
		//if ctrl char
		if(ctrl > 0){
			buff = (strcat(buff, "~~~"));
			char* ctrlptr = (char*) malloc(sizeof(char)*2);
			ctrlptr[0] = controlConvert(code_arr[i]->string[0]);
			ctrlptr[1] = '\0';
			buff = (strcat(buff, ctrlptr));
			free(ctrlptr);
		}else{
			buff = strcat(buff, code_arr[i]->string);
		}

		buff = strcat(buff, "\n");
		
		write(fd, buff, buff_len-1);
		free(buff);
		count++;
	}
	close(fd);

	return count;
}

void free_tree(node tree){
	
	if(tree == NULL) return;

	free_tree(tree->left);
	free_tree(tree->right);
	
	free(tree->string);
	free(tree);
}

void build_code(node heap, int i, int n){
	
	//check for node string. null string means leaf
	if((heap->string != NULL) && (heap->left == NULL) && (heap->right == NULL)){
		if(code_temp == NULL){
			code_arr[code_ind]->string = heap->string;
			code_arr[code_ind]->code[0] = '0';
			code_arr[code_ind]->code[1] = '\0';
			return;
		}
		code_arr[code_ind]->string = heap->string;
		char* c_temp = (char*) malloc(sizeof(char)*(n+1));
		c_temp = strcpy(c_temp, code_temp);
		memset(c_temp + i, '\0', (n+1)-(i+1));
		code_arr[code_ind]->code = c_temp;
		code_ind++;
		return;
	}

	if(heap->left != NULL){
		code_temp[i] = '0';
		build_code(heap->left, i+1, n);
	}
	if(heap->right != NULL){
		code_temp[i] = '1';
		build_code(heap->right, i+1, n);
	}
}

node pop(){
	
	//pop min
	if(min_heap[0] != NULL){
		int i;
		node ret = min_heap[0];
		min_heap[0] = min_heap[null_spot-1];
		min_heap[null_spot-1] = NULL;
		--null_spot;
		//resift heap
		fix_heap(0);
		return ret;
	}
	return NULL;
}

void fix_heap(int ind){
	
	if(min_heap[ind] == NULL) return;
	
	//children
	int left = (2*ind)+1;
	int right = (2*ind)+2;
	
	//index of lowest freq child
	int least = ind;
	
	//left check for least
	if(left < null_spot){
		if(min_heap[left]->freq < min_heap[ind]->freq){
			least = left;
		}
	}
	//right check for least
	if(right < null_spot){
		if(min_heap[right]->freq < min_heap[least]->freq){
			least = right;
		}
	}
	//if least isnt the parent continue with recursive call
	if(least != ind){
		node temp = min_heap[ind];
		min_heap[ind] = min_heap[least];
		min_heap[least] = temp;
		fix_heap(least);
	}
}

int build_tree(){

	int count = 0;
	
	//while at least two nodes in heap
	while(null_spot > 1){
		node first = pop();
		node second = pop();

		//create new node with only freq
		node tree = (node) malloc(sizeof(struct node));
		tree->freq = first->freq + second->freq;
		tree->string = NULL;
		tree->left = first;
		tree->right = second;
	
		insert_heap(tree);

		count++;
		
	}
	return count;
}

void sift_up(node n){
	
	int ind = null_spot;
	
	//while true cuz idk.
	while(1){
		if(n->freq < min_heap[(ind-1)/2]->freq){
			node temp = min_heap[(ind-1)/2];
			min_heap[(ind-1)/2] = n;
			min_heap[ind] = temp;
			ind = (ind-1)/2;
		}else{
			return;
		}
		
	}
}

void insert_heap(node n){
	
	if(min_heap == NULL){
		min_heap = (node*) malloc(sizeof(node)*1);
		min_heap[0] = NULL;
		heap_size = 1;
		null_spot = 0;
	}

	//if heap is full
	if(null_spot == heap_size){

		//double heap size
		node* temp_heap = (node*) malloc(sizeof(node)*(heap_size*2));
		
		//copy old heap
		int i;
		for(i=0; i<heap_size; i++){
			temp_heap[i] = min_heap[i];	
		}
		
		free(min_heap);
		min_heap = temp_heap;

		heap_size = heap_size*2;
	}
	//insert into heap
	min_heap[null_spot] = n;
	sift_up(n);
	++null_spot;
}

int insert(char* token) {
	node n1 = NULL;
	int hash = 0;
	node temp = NULL;

	//create the hash table if it doesnt exists
	//if ht doesnt exist, heap doesnt exist - create heap
	if(table == NULL) {
		table = (struct node **) malloc(sizeof(struct node *)*1);
		tableSize = 1;
		keys = 0;
		table[0] = NULL;
	}

	//hash functions is created with the formula below
	hash = strlen(token) % tableSize;
	temp = table[hash];
	
	//checks if the index result by the hash is null
	if(temp != NULL) {
		//traverses through the nodes in the index (these nodes are treated as a LL)
		while(temp -> right != NULL) {
			//Checks the nodes to see if the token already exists,
			//which then increments the freq by 1 if it does
			if(strcmp(temp -> string,token) == 0) {
				temp -> freq++;
				return 2;
			}
			temp = temp -> right;
		}
		//Checks the last node in the LL to see if it is a existing token
		if(strcmp(temp -> string, token) == 0) {
			temp -> freq++;
			return 2;
		}
		//Otherwise, creates a new node for the token, and inserts it to the end of the LL
		else {
			n1 = (node) malloc(sizeof(struct node));
			n1 -> string = token;
			n1 -> freq = 1;
			n1 -> left = NULL;
			n1 -> right = NULL;
			keys++;
			temp -> right = n1;
			n1 -> left = head;
			head = n1;
			
		}
	}
	//Which then proceeds to just insert the node into the index here
	else {
		n1 = (node) malloc(sizeof(struct node));
		n1 -> string = token;
		n1 -> freq = 1;
		n1 -> left = NULL;
		n1 -> right = NULL;
		keys++;
		table[hash] = n1;
		n1 -> left = head;
		head = n1;

	}
	temp = NULL;
	n1 = NULL;
	//Checks the load factor, if it is greater then 0.75, calls rehash
	if( ((double) keys / (double) tableSize) > 0.75) {
		rehash();
	}

	return 1;
}

void rehash() {
	int oldTableSize = tableSize;
	tableSize = tableSize * 2;
	struct node ** tempTable = (struct node **) malloc(sizeof(struct node *)*tableSize);
	node iterNode = NULL;
	node tempNode = NULL;
	node prevNode = NULL;
	int hash = 0;
	char* str;
	int i = 0;


	while(i < tableSize) {
		tempTable[i] = NULL;
		i++;
	}
	
	i = 0;
	//Goes through the old table, and starts putting in each of
	//the nodes back into the tempTable (the new table)
	while(i < oldTableSize) {
		tempNode = table[i];
		prevNode = NULL;

		while(tempNode != NULL) {

			hash = strlen(tempNode -> string) % tableSize;
			iterNode = tempTable[hash];
			if(iterNode == NULL) {
				tempTable[hash] = tempNode;
			}
			else {
				while(iterNode -> right != NULL) {
					iterNode = iterNode -> right;
				}

				iterNode -> right = tempNode;
			}
			prevNode = tempNode;
			tempNode = tempNode -> right;
			prevNode -> right = NULL;
		}
		i++;
	}
	free(table);
	table = tempTable;

	return;

}

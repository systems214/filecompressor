#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <fcntl.h>
#include<errno.h>
#include<dirent.h>
#include"build.h"
#include"compress.h"
#include"decompress.h"
#include"utils.h"

char parse_flag(char*);
int check_flags(char*);
int recursive_operation(char*, int, int);

/*
node* min_heap = NULL;
int heap_size = NULL;
int null_spot = NULL;

codes* code_arr = NULL;
int code_ind = 0;
char* code_temp = NULL;

node* table = NULL;
int tableSize = 0;
int keys = 0;

node head = NULL;
//*/

int main(int argc, char** argv){

	//error checking argv
	if(argc<3){
		printf("Error: Not enough arguments detected.\n");
		return -1;
	}
	if(argc>5){
		printf("Error: Too many arguments detected.\n");
		return -1;
	}

	char flags[2];
	memset(flags, '\0', 2);
	
	//parsed flags
	flags[0] = parse_flag(argv[1]);
	flags[1] = parse_flag(argv[2]);

	//flags check
	//0 || -1 if error. 1-6 otherwise for flag combos
	int check = check_flags(flags);
	
	//flag error checks (and check_flags function error check)
	if(check == -1) return -1;
	if(check == 0){
		printf("check_flags went wrong\n");
		return -1;
	}
	
	//file descriptor
	int fd;

	//codebook
	int hcode_fd = -2;

	int count = 0;

	int i;
	
	//non=recursive
	if(check <= 3){

		if(argc > 4){
			printf("Error: Too many arguments\n");
			return -1;
		}
		
		fd = open(argv[2], O_RDONLY);
				
		if(errno == EISDIR){
			printf("Error: Path is a directory\n");
			return -1;
		}
		if(errno == ENOENT){
			printf("Error: Path does not exist\n");
			return -1;
		}

		switch(check){
			
			//build
			case 1: 
				
				if(argc > 3){
					printf("Error: Too many arguments for flag -b\n");
					return -1;
				}
				
				//build heap and tree
				count = build_table(fd);
				build_heap();
				build_tree();
				
				//initialize array of codes
				code_arr = (codes*) malloc(sizeof(struct codes)*keys);
				code_temp = (char*) malloc(sizeof(char)*keys+1);
				memset(code_temp, '\0', keys+1);
				
				for(i=0; i<keys; i++){
					code_arr[i] = (codes) malloc(sizeof(struct codes));
					code_arr[i]->string = NULL;
					code_arr[i]->code = NULL;
				}
				//build huffman codes
				build_code(min_heap[0], 0, keys);
				//printf("Codebook for %d tokens created\n", keys);	
				
				/*
				for(i=0; i<count; i++){
					printf("String: %s code: %s\n", code_arr[i]->string, code_arr[i]->code);
				}
				*/

				write_codebook(keys);

				free(code_temp);

				for(i=0; i<keys; i++){
					free(code_arr[i]->code);
					free(code_arr[i]);
				}
				free(code_arr);
			
				free_tree(min_heap[0]);
				free(min_heap);
				free(table);
				return 0;
			
			//compress
			case 2: 
				
				if(argc < 4){
					printf("Error: Not enough arguments\n");
					return -1;
				}

				hcode_fd = open(argv[3], O_RDONLY);
				
				count = compress(fd, argv[2], hcode_fd);
			//	printf("Compressed %d file\n", count);
			
				return 0;

			//decompress
			case 3: 
				
				if(argc < 4){
					printf("Error: Not enough arguments\n");
					return -1;
				}

				hcode_fd = open(argv[3], O_RDONLY);
				loadBook(hcode_fd);

				count = decompress(fd, argv[2], hcode_fd);
			//	printf("Decompressed %d file\n", count);
				freecodebook();
				return 0;
		}
	
	//Recursive
	}else if(check >= 4){
		
		//error checks
		if((argc < 4)){
			printf("Error: Not enough arguments\n");
			return -1;
		}
		if(check != 4 && argc < 5){
			printf("Error: Not enough arguments\n");
			return -1;
		}
		
		char* dir_name = NULL;
		
		//for -c and -d flags
		if(check > 4){
			hcode_fd = open(argv[4], O_RDONLY);
			if(check == 6) {
				loadBook(hcode_fd);
			}
			if(errno == EISDIR){
				printf("Error: Codebook not found. Path is a directory\n");
				return -1;
			}
			if(errno == ENOENT){
				printf("Error: Codebook not found. Path does not exist\n");
				return -1;
			}
			
			dir_name = argv[3];
		}

		//-b flag
		if(check == 4){
			dir_name = argv[3];
		}

		//returns number of files executed flag on
		int count = recursive_operation(dir_name, check, hcode_fd);
		
		//-b flag
		if(check == 4){
			build_heap();
			build_tree();

			//initialize array of codes
			code_arr = (codes*) malloc(sizeof(struct codes)*keys);
			code_temp = (char*) malloc(sizeof(char)*keys+1);
			memset(code_temp, '\0', keys+1);
			
			for(i=0; i<keys; i++){
				code_arr[i] = (codes) malloc(sizeof(struct codes));
				code_arr[i]->string = NULL;
				code_arr[i]->code = NULL;
			}
			//build huffman codes
			build_code(min_heap[0], 0, keys);
				
			write_codebook(keys);

			free(code_temp);

			for(i=0; i<keys; i++){
				free(code_arr[i]->code);
				free(code_arr[i]);
			}
			free(code_arr);
		
			free_tree(min_heap[0]);
			free(min_heap);
			free(table);
		}
		else if(check == 6) {
			freecodebook();
		}
		/*usage strings for tests
		if(check == 4){
			printf("Codebook built for %d file(s)\n", keys);
		}else if(check == 5){
			printf("Compressed %d file(s)\n", count);
		}else if(check == 6){
			printf("Decompressed %d file(s)\n", count);
		}
		//*/
	}

	return 0;
}

int recursive_operation(char* dir_name, int flag, int hcode_fd){
	
	//full path of file because opendir is dumb and doesnt open from the
	//current DIR. Dumb.
	//+2 for / and \0
	
	char* full_path = NULL;
	if(dir_name[strlen(dir_name)-1] == '/'){
		full_path = (char*) malloc(sizeof(char)*strlen(dir_name)+1);
		memset(full_path, '\0', strlen(dir_name)+1);
		full_path = strcat(full_path, dir_name);
	}else{
		full_path = (char*) malloc(sizeof(char)*strlen(dir_name)+2);
		memset(full_path, '\0', strlen(dir_name)+2);
		full_path = strcat(full_path, dir_name);
		full_path = strcat(full_path, "/");
	}

	DIR* dirp = opendir(full_path);
	
	//error checks for dirp
	if(errno == ENOTDIR){
		printf("Error: Path is not a directory\n");
		return -1;
	}
	if(errno == ENOENT){
		printf("Error: Path does not exist\n");
		return -1;
	}

	//directory?
	struct dirent* dir;
	int count = 0;
	
	//read thru files in directory
	while((dir=readdir(dirp)) != NULL){
		//if dir go into dir
		if(dir->d_type == DT_DIR){
			
			if(strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0){
				continue;
			}
			
			int full_path_len = strlen(full_path);
			int subdir_len = strlen(dir->d_name);
			char* temp = (char*) malloc(sizeof(char)*(full_path_len + subdir_len + 1));
			memset(temp, '\0', full_path_len + subdir_len +1);
			
			temp = strcpy(temp, full_path);
			temp = strcat(temp, dir->d_name);

			count = count + recursive_operation(temp, flag, hcode_fd);
			free(temp);
			continue;
		}

		//if file, call operation
		if(dir->d_type == DT_REG){
			int fd = 0;

			char* filename = dir->d_name;

			//resize path length
			int filename_len = strlen(filename);
			int full_path_len = strlen(full_path);
			//+2 for / and \0
			char* temp = (char*) malloc(sizeof(char)*(filename_len + full_path_len + 2));
			memset(temp, '\0', filename_len + full_path_len+2);

			temp = strcpy(temp, full_path);
			if(full_path[strlen(full_path)-1] != '/'){
				temp = strcat(temp, "/");
			}
			temp = strcat(temp, filename);

			switch(flag){

				case 4: 
					fd = open(temp, O_RDONLY);
					count = count + build_table(fd);
					break;

				case 5:
					fd = open(temp, O_RDONLY);
					count = count + compress(fd, temp, hcode_fd);
					break;

				case 6:
					fd = open(temp, O_RDONLY);
					count = count + decompress(fd, temp, hcode_fd);
					break;
			}
			free(temp);
			continue;
		}
	}
	closedir(dirp);
	free(full_path);
	return count;
}

int check_flags(char* flags){
	switch (flags[0] + flags[1]){

	//errors
	case '\0': 
		printf("Error: No flags detected\n"); 
		return -1;

	case 'R': 
		printf("Error: No operating mode detected with Recursive flag\n");
		return -1;		
	
	//valid
	case 'b':	return 1;
	case 'c':	return 2;
	case 'd':	return 3;
	case 'R'+'b': return 4;
	case 'R'+'c': return 5;
	case 'R'+'d': return 6;
	
	//other error combinations
	default:
		printf("Error: Multiple operating modes or invalid modes detected\n");
		return -1;
	}
	return 0;
}

char parse_flag(char* flag){

	if(strncmp(flag, "-", 1) == 0){
		switch (flag[1]){
			
			case 'R': return 'R';
			case 'b': return 'b';
			case 'c': return 'c';
			case 'd': return 'd';
			default: return '\0';
		}
	}else{
		//not a flag
		return '\0';
	}
}






#ifndef _build_h

#define _build_h

struct node{
	
	int freq;
	char* string;

	struct node* left;
	struct node* right;
};

struct codes{
	char* code;
	char* string;
};

typedef struct node* node;
typedef struct codes* codes;

  node* min_heap;
  int heap_size;
  int null_spot;

  codes* code_arr;
  int code_ind;
  char* code_temp;

  node* table;
  int tableSize;
  int keys;

  node head;

int build_heap();
int build_tree();
void build_codes(node, int, int);
int write_codebook(int);

#endif

all:fileCompressor

fileCompressor: fileCompressor.c build.c compress.c decompress.c utils.c
	gcc -O fileCompressor.c build.c compress.c decompress.c utils.c -o fileCompressor

clean:
	rm -rf fileCompressor

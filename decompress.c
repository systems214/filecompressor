#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<fcntl.h>
#include"utils.h"
#include"decompress.h"

int decompress(int fd, char* filename, int codebook) {
	char* str = (char *) malloc(sizeof(char)*2);
	char* temp = NULL;
	char letter = 1;
	int index = 0;
	int max = 2;
	char buff = 0;
	int fileNameSize = 0;
	int length = 0;
	char* UncompressedName = NULL;
	memset(str,'\0',2);
	int fdUncompressed = 0;
	int i = 0;
	if(fd <= 0 || codebook <= 0 || filename == NULL) {
		return 0;
	}

	if( (filename[strlen(filename)-4] != '.') || (filename[strlen(filename)-3] != 'h') && (filename[strlen(filename)-2] != 'c') && (filename[strlen(filename)-1] != 'z') ) {
		free(str);
		return 0;
	}
	
	while(letter != '\0') {
		 letter = filename[length];
		 length++;
	}

	fileNameSize = (length-4);
	UncompressedName = (char *) malloc(sizeof(char)*fileNameSize);
	//Zero out UncompressedName
	memset(UncompressedName,'\0',(fileNameSize));
	for(i = 0; i < (fileNameSize-1); i++) {
		UncompressedName[i] = filename[i];
	}

	//Create the file
	fdUncompressed = creat(UncompressedName, 0640);
	if(fdUncompressed < 0) {
		return 0;
	}
	
	free(UncompressedName);

	
	while(read(fd,&buff,1) > 0) {
		str[index] = buff;
		index++;
		if( (index+1) >= max) {
			max = max * 2;
			temp = realloc(str,max);
			while(temp == NULL) {
				printf("Waiting for realloc to return\n");
			}
			str = temp;
			temp = NULL;
			memset(str+(index+1), '\0', max-(index+1));
		}
		if(grabHuffD(str,fdUncompressed) > 0) {
			memset(str,'\0',max);
			index = 0;
		}
	}
	close(fdUncompressed);
	close(fd);

	if(str != NULL) free(str);
	return 1;
}

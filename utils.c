#include<stdlib.h>
#include<stdio.h>
#include<fcntl.h>
#include<string.h>
#include"utils.h"


char controlConvert(char letter) {
	if(iscntrl(letter) == 0) {
		switch(letter) {
			case 'a' :
				return '\a';
			case 'b' :
				return '\b';
			case 'e' :
				return '\e';
			case 'f' :
				return '\f';
			case 'n' :
				return '\n';
			case 'r' :
				return '\r';
			case 't' :
				return '\t';
			case 'v' :
				return '\v';
			case '?' :
				return '\?';
			default :
				return 0;
		}

	}
	else {

		switch(letter) {
			case '\a' :
				return 'a';
			case '\b' :
				return 'b';
			case '\e' :
				return 'e';
			case '\f' :
				return 'f';
			case '\n' :
				return 'n';
			case '\r' :
				return 'r';
			case '\t' :
				return 't';
			case '\v' :
				return 'v';
			case '\?' :
				return '\?';
			default :
				return 0;
		}
	}
	return 0;
}
int loadBook(int codebook) {
	char** temp_heap = NULL;
	char* huffCode = (char *) malloc(sizeof(char)*2);
	char* codebookToken = (char *) malloc(sizeof(char)*2);
	char* specialChar = NULL;
	char sChar = 0;
	memset(huffCode,'\0',2);
	memset(codebookToken,'\0',2);
	char buff = 0;
	int index1 = 0;
	int index2 = 0;
	int index = 0;
	int max1 = 2;
	int max2 = 2;
	char* temp = NULL;
	int count = 0;
	int readBytes = 2;
	int sCharLength = 0;
	lseek(codebook, 0, SEEK_SET);
	struct stat tempstruct;
	fstat(codebook,&tempstruct);
	int size = tempstruct.st_size;
	int i = 0;

	if(codebookHeap == NULL) {
		cSize = 1;
		codebookHeap = (char**) malloc(sizeof(char*)*cSize);
		for(i = 0; i < cSize;i++) {
			codebookHeap[i] = NULL;
		}
	}

	//Read the special char if it exists. If it does not, then grabHuff will do nothing
	if(read(codebook,&buff,1) > 0) {
		count++;
		//Checking if a special character does indeed exists. A special character is defined as:
		//Not a space
		//Not a control character
		//It will repeat over
		//For example, ~~~ is our special character of choice, however, this code should be able to read codebooks with a special character of !!! or ***
		//A special character like !@! or %%$ does not count as a special character, and therefore produce an error
		if(buff == ' ' || iscntrl(buff) > 0 ) {
			free(huffCode);
			free(codebookToken);
			if(huffCode != NULL) huffCode = NULL;
			if(codebookToken != NULL) codebookToken = NULL;
			//fprintf(stderr, "No special Characters were found\n");
			return 0;
		}
		else {
			sChar = buff;
			sCharLength++;
			while(read(codebook, &buff, 1) > 0) {
				count++;
				if(buff == '\n') {
					break;
				}
				//A different character was found, terminating grabHuff
				if(buff != sChar) {
					free(huffCode);
					free(codebookToken);
					if(huffCode != NULL) huffCode = NULL;
					if(codebookToken != NULL) codebookToken = NULL;
					//fprintf(stderr, "A invalid special character was found\n");
					return 0;
				}
				else {
					//Increment the length of our special character
					sCharLength++;
				}
			}
			if(sCharLength > 0) {
				//Create the string that contains our special character
				specialChar = (char *) malloc(sizeof(char)*(sCharLength+1));
				memset(specialChar,sChar,sCharLength);
				specialChar[sCharLength] = '\0';
			}
			else {
				//there was no characters for special Character, return wil do nothing
				free(huffCode);
				free(codebookToken);
				if(huffCode != NULL) huffCode = NULL;
				if(codebookToken != NULL) codebookToken = NULL;
				//fprintf(stderr, "No special characters were found\n");
				return 0;
			}
		}
	}
	//While the codebook isnt empty
	while(count < size) {
		//while the codebook is not empty, we read a char byte from it and store it into buff
		while(read(codebook, &buff,1)> 0) {
			count++;
			//if the buffer is a tab, that means we are done reading the huffman code version
			if(buff == '\t') {
				break;
			}
			//Add onto our huffCode, which houses our crypted string of the codebookToken
			huffCode[index1] = buff;
			index1++;
			//If the huffCode is exceeded/reached its maximum space, double the size
			if( (index1+1) >= max1) {
				//Double the size, as usual
				max1 = max1 * 2;
				temp = realloc(huffCode,max1);
				while(temp == NULL) {
					printf("Waiting for realloc to return\n");
				}
				huffCode = temp;
				temp = NULL;
				memset(huffCode+(index1+1),'\0', max1-(index1+1));
			}
		}


		//For creating the heap;
		for(i = 0; i < index1; i++) {
			if(huffCode[i] == '0') {
				index = ((2*index) +1);
			}
			else {
				index = ((2*index) +2);
			}
		}
		if(index >= cSize) {
			temp_heap = (char**) malloc(sizeof(char*) * (index+1));
			for(i = 0; i < cSize; i++) {
				temp_heap[i] = codebookHeap[i];
			}
			for(i = cSize; i <= index;i++) {
				temp_heap[i] = NULL;
			}
			free(codebookHeap);
			codebookHeap = temp_heap;
			temp_heap = NULL;
			cSize = (index+1);
		}

		//reset the variables we need
		temp = NULL;
		
		//now we try to get the codebookToken itself
		while( read(codebook, &buff, 1) > 0) {
			count++;
			//if we get to new line, this means we have both the huffCode, the compressed token, and codebookToken, the uncompressed token
			if(buff == '\n') {
				//if the token is the length of our special character + 1 (this means that it could potentially be a escape command)
				if(index2 == (sCharLength+1)) {
					//check the first bytes of the codebookToken to see if they match our special character
					for(i = 0; i < sCharLength; i++) {
						if(codebookToken[i] != specialChar[i]) {
							//The first bytes do not match, so we just treat it as any other token (that is not an escape character)
							i = -1;
							break;
						}
					}
					if(i == -1) {
						break;
					}
					else {
						//check to see if the last character is a valid control character
						buff = controlConvert(codebookToken[sCharLength]);
						if(iscntrl(buff) != 0) {
							//replace our codetoken with a control character
							codebookToken[0] = buff;
							index2 = 1;
							memset(codebookToken+1, '\0', max2-1);
						}
					}
				}
				//We are done, we need to check if the codebookToken matches with our token
				break;
			}
			//Add the character to our codebookToken
			codebookToken[index2] = buff;
			index2++;
			//Chec kif codebookToken exceeded/reached max size
			if( (index2+1) >= max2) {
				max2 = max2 * 2;
				temp = realloc(codebookToken,max2);
				while(temp == NULL) {
					printf("Waiting for realloc to return \n");
				}
				codebookToken = temp;
				memset(codebookToken+(index2+1), '\0', max2-(index2+1));
				temp = NULL;
			}
		}

		codebookHeap[index] = codebookToken;
		numTokens++;
		
		memset(huffCode,'\0',max1);
		codebookToken = (char *) malloc(sizeof(char)*max2);
		memset(codebookToken,'\0',max2);
		index1 = 0;
		index2 = 0;
		index = 0;
	}
	//This means that the token does not exists in our codebook, and grabHuff does nothing
	if(huffCode != NULL) free(huffCode);
	if(codebookToken != NULL) free(codebookToken);
	if(specialChar != NULL) free(specialChar);
	return 0;
}

void freecodebook() {
	char * temp = NULL;
	int i = 0;
	for(i = 0; i < cSize; i++) {
		temp = codebookHeap[i];
		if(temp != NULL) {
			free(temp);
			temp = NULL;
		}
	}
	if(codebookHeap != NULL) {
		free(codebookHeap);
	}
	return;

}

int grabHuffD(char* token, int fd) {
	int i = 0;
	int index = 0;
	while(token[i] != 0){
		if(token[i] == '0') {
			index = ((2*index) +1);
		}
		else {
			index = ((2*index) +2);
		}
		i++;
	}
	if(codebookHeap[index] != NULL) {
		write(fd,codebookHeap[index],strlen(codebookHeap[index]));
		return 1;
	}
	//fprintf(stderr, "Token was not found in codebook\n");
	return 0;
}


int loadC(int codebook) {
	char* huffCode = (char *) malloc(sizeof(char)*2);
	char* codebookToken = (char *) malloc(sizeof(char)*2);
	char* specialChar = NULL;
	char** temp_book = NULL;
	char** temp_book2 = NULL;
	char sChar = 0;
	memset(huffCode,'\0',2);
	memset(codebookToken,'\0',2);
	char buff = 0;
	int index1 = 0;
	int index2 = 0;
	int max1 = 2;
	int max2 = 2;
	char* temp = NULL;
	int count = 0;
	int readBytes = 2;
	int sCharLength = 0;
	lseek(codebook, 0, SEEK_SET);
	struct stat tempstruct;
	fstat(codebook,&tempstruct);
	int size = tempstruct.st_size;
	int i = 0;
	int index = 0;

	if(cbookToken == NULL && cbookCode == NULL) {
		cSize = 1;
		cbookToken = (char **) malloc(sizeof(char*)*cSize);
		cbookCode = (char **) malloc(sizeof(char*)*cSize);
	}
	

	//Read the special char if it exists. If it does not, then grabHuff will do nothing
	if(read(codebook,&buff,1) > 0) {
		count++;
		//Checking if a special character does indeed exists. A special character is defined as:
		//Not a space
		//Not a control character
		//It will repeat over
		//For example, ~~~ is our special character of choice, however, this code should be able to read codebooks with a special character of !!! or ***
		//A special character like !@! or %%$ does not count as a special character, and therefore produce an error
		if(buff == ' ' || iscntrl(buff) > 0 ) {
			free(huffCode);
			free(codebookToken);
			if(huffCode != NULL) huffCode = NULL;
			if(codebookToken != NULL) codebookToken = NULL;
			//fprintf(stderr, "No special Characters were found\n");
			return 0;
		}
		else {
			sChar = buff;
			sCharLength++;
			while(read(codebook, &buff, 1) > 0) {
				count++;
				if(buff == '\n') {
					break;
				}
				//A different character was found, terminating grabHuff
				if(buff != sChar) {
					free(huffCode);
					free(codebookToken);
					if(huffCode != NULL) huffCode = NULL;
					if(codebookToken != NULL) codebookToken = NULL;
					//fprintf(stderr, "A invalid special character was found\n");
					return 0;
				}
				else {
					//Increment the length of our special character
					sCharLength++;
				}
			}
			if(sCharLength > 0) {
				//Create the string that contains our special character
				specialChar = (char *) malloc(sizeof(char)*(sCharLength+1));
				memset(specialChar,sChar,sCharLength);
				specialChar[sCharLength] = '\0';
			}
			else {
				//there was no characters for special Character, return wil do nothing
				free(huffCode);
				free(codebookToken);
				if(huffCode != NULL) huffCode = NULL;
				if(codebookToken != NULL) codebookToken = NULL;
				//fprintf(stderr, "No special characters were found\n");
				return 0;
			}
		}
	}
	//While the codebook isnt empty
	while(count < size) {
		//while the codebook is not empty, we read a char byte from it and store it into buff
		while(read(codebook, &buff,1)> 0) {
			count++;
			//if the buffer is a tab, that means we are done reading the huffman code version
			if(buff == '\t') {
				break;
			}
			//Add onto our huffCode, which houses our crypted string of the codebookToken
			huffCode[index1] = buff;
			index1++;
			//If the huffCode is exceeded/reached its maximum space, double the size
			if( (index1+1) >= max1) {
				//Double the size, as usual
				max1 = max1 * 2;
				temp = realloc(huffCode,max1);
				while(temp == NULL) {
					printf("Waiting for realloc to return\n");
				}
				huffCode = temp;
				temp = NULL;
				memset(huffCode+(index1+1),'\0', max1-(index1+1));
			}
		}


		//reset the variables we need
		temp = NULL;
		

		//now we try to get the codebookToken itself
		while( read(codebook, &buff, 1) > 0) {
			count++;
			//if we get to new line, this means we have both the huffCode, the compressed token, and codebookToken, the uncompressed token
			if(buff == '\n') {
				//if the token is the length of our special character + 1 (this means that it could potentially be a escape command)
				if(index2 == (sCharLength+1)) {
					//check the first bytes of the codebookToken to see if they match our special character
					for(i = 0; i < sCharLength; i++) {
						if(codebookToken[i] != specialChar[i]) {
							//The first bytes do not match, so we just treat it as any other token (that is not an escape character)
							i = -1;
							break;
						}
					}
					if(i == -1) {
						break;
					}
					else {
						//check to see if the last character is a valid control character
						buff = controlConvert(codebookToken[sCharLength]);
						if(iscntrl(buff) != 0) {
							//replace our codetoken with a control character
							codebookToken[0] = buff;
							memset(codebookToken+1, '\0', max2-1);
							index2 = 1;
						}
					}
				}
				//We are done, we need to check if the codebookToken matches with our token
				break;
			}
			//Add the character to our codebookToken
			codebookToken[index2] = buff;
			index2++;
			//Chec kif codebookToken exceeded/reached max size
			if( (index2+1) >= max2) {
				max2 = max2 * 2;
				temp = realloc(codebookToken,max2);
				while(temp == NULL) {
					printf("Waiting for realloc to return \n");
				}
				codebookToken = temp;
				memset(codebookToken+(index2+1), '\0', max2-(index2+1));
				temp = NULL;
			}
		}

		cbookToken[index] = codebookToken;
		cbookCode[index] = huffCode;
		index++;
		numTokens++;
		if(index >= cSize) {
			temp_book = (char **) malloc(sizeof(char*)*(cSize*2));
			temp_book2 = (char **) malloc(sizeof(char*)*(cSize*2));
			for(i = 0; i < cSize; i++) {
				temp_book[i] = cbookToken[i];
				temp_book2[i] = cbookCode[i];
			}
			for(i = cSize; i < (cSize*2);i++) {
				temp_book[i] = NULL;
				temp_book2[i] = NULL;
			}
			free(cbookToken);
			free(cbookCode);
			cbookToken = temp_book;
			cbookCode = temp_book2;
			temp_book = NULL;
			temp_book2 = NULL;
			cSize = cSize *2;

		}
		//Otherwise, we reset our codebookToken and huffcode, and proceed to do it again
		huffCode = (char *) malloc(sizeof(char)*max1);
		memset(huffCode,'\0',max1);
		codebookToken = (char*) malloc(sizeof(char)*max2);
		memset(codebookToken,'\0',max2);
		index1 = 0;		
		index2 = 0;
	}
	//This means that the token does not exists in our codebook, and grabHuff does nothing
	if(huffCode != NULL) free(huffCode);
	if(codebookToken != NULL) free(codebookToken);
	if(specialChar != NULL) free(specialChar);
	//fprintf(stderr, "Token was not found in codebook\n");

	return 0;
}


int grabHuffC(char* token, int fd) {
	int i = 0;

	for(i = 0; i < cSize; i++) {
		if(strcmp(cbookToken[i],token) == 0) {
			write(fd,cbookCode[i],strlen(cbookCode[i]));
			return 1;
		}
	}

	//fprintf(stderr, "Token was not found in codebook\n");

	return 0;
}


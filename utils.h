#ifndef _utils_h

#define _utils_h

char controlConvert(char);
int grabHuffC(char*, int);
int grabHuffD(char*, int);
int loadBook(int);
int loadC(int);
void freecodebook();
char** codebookHeap;
char** cbookToken;
char** cbookCode;
int cSize;
int numTokens;

#endif
